# network
import os
import requests
import setting

""" sign up and registeration part ......................................... """
def is_valid_name(name):
    if 0 <= len(name) <= 100:
        return True
    return False

def is_valid_username(username):
    if (3 <= len(username) <= 100
        and username.replace('_', '').isalnum()
        and (username[0].isalpha() or username[0] == '_')
):
        return True
    return False

def is_valid_password(password):
    if len(password) >= 8:
        return True
    return False

def registeration(name, username, password):
    sms = ""
    payload = dict(name=name, username=username, password=password)
    try:
        r = requests.post(setting.urls['signup'], data=payload)
    except requests.exceptions.ConnectionError:
        return False, ("Connection failed! Try again."+r.text)
    if r.ok:
        return True, r.text
    elif r.status_code == 400: 
        return False, r.text
    else:
        return False, ("Unknown error occured."+r.text)
    
def signup(session):
    sms = ""
    finish = False
    while True:
        os.system("clear")
        if sms: print(sms)
        if finish:
            input("***Registeration was successfull ***\nPress enter to go back to menu")
            return
        name = str(input("Enter your name: "))
        if not is_valid_name(name):
            sms = "Enter a valid name please"
            continue
        username = str(input("Enter your username: "))
        if not is_valid_username(username):
            sms = "Your username must only contain letters, numbers, and underscores. It may not start with a number."
            continue
        password = str(input("Enter your password: "))
        if not is_valid_password(password):
            sms = "Your username must only contain letters, numbers, and underscores. It may not start with a number."
            continue
        repassword = str(input("Enter your password again: "))
        if repassword != password:
            sms = "Your password repeat was not the same"
            continue
        finish, sms = registeration(name, username, password) 
        
""" sign in part ........................................................... """
def is_username_valid(username):
    if username:
        return True
    return False

def is_password_valid(password):
    if password:
        return True
    return False

def login(username, password, session):
    payload = dict(username=username, password=password)
    try:
        r = session.post(setting.urls['signin'], data=payload)
    except requests.exceptions.ConnectionError:
        return False, "Connection failed!"
    if r.ok:
        return True, r.text
    elif r.status_code == 400 or r.status_code == 403:
        return False, r.text
    else:
        return False, "Unknown error occured."

def signin(session):
    sms = ""
    finish = False
    try:
        while True:
            os.system("clear")
            if sms: print(sms + "\n")
            if finish:
                input("*** Your singing in was successfull!***\nNow press enter to return to the menu.\n")
                return True
            username = str(input("Enter your username: "))
            if not is_username_valid(username):
                sms = "empty!"
                continue
            password = str(input("Enter your password: "))
            if not is_password_valid(password):
                sms = "empty!"
                continue
            finish, sms = login(username, password, session)
    except KeyboardInterrupt:
        return False

""" log out part ........................................................... """
def signout(session):
    try:
        r = session.get(setting.urls["signout"])
                      
    except requests.exceptions.ConnectionError:
        return True,"Connection failed!"
                      
    if r.ok or r.status_code == 400:
        return False, r.text

    else:
        return True, "Unknown error occured."

def logout(session):
    os.system("clear")
    finish = True
    sms = ""
    try:
        finish, sms = signout(session)
        print(sms + "\n")
        input("Press enter to go back")
        return finish

    except KeyboardInterrupt:
        return finish
    
""" dummy part ............................................................. """
def webserver_connection(session):
    try:
        r = session.get(setting.urls["dummy"])
    except requests.exceptions.ConnectionError:
        return False, "Connection failed!"
    if r.ok:
        return True, r.text
    elif r.status_code == 403:
        return False, r.text
    else:
        return False, "Unknown error occured."



def dummy(session):
    sms = ""
    os.system("clear")
    finish = False
    try:
        finish, sms = webserver_connection(session)
        print(sms + "\n")
        input("Press enter to go back")
        return finish
    except KeyboardInterrupt:
        return finish
    
    
