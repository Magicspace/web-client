#The menu
import network
import requests
import os

def is_valid_menu(s):
    return s in ["1","2","3"]

"""  start the menu """
def init():
    pm=""
    session=requests.session()
    is_loggedin=False
    while True:
        os.system("clear")
        if pm != "": print(pm)
        pm = ""
        if not is_loggedin:
            print("What can I do for you enter the number:\n")
            print("1. Sign up")
            print("2. Sign in")
            print("3. Exit\nYour number:")
            x = str(input())
            if is_valid_menu(x):
                if x =='1':
                    network.signup(session)
                elif x == '2':
                    is_loggedin = network.signin(session)
                    if not is_loggedin:
                            pm = "Your are not logged in!"
                    else:
                        pm = "Welcome!"
                else:
                    print("Goodbye!")
                    break
            else:
                pm = "Enter 1, 2, or 3"
        else:
            print("You are logged in now ;) \nWhat can I do for you?\n")
            print("1. Dummy page")
            print("2. Log out")
            print("3. Exit\n")
            x = str(input("your number:"))
            if is_valid_menu(x):
                if x == '1':
                    pm = network.dummy(session)
                    if not pm:
                        pm = "prosses failed!"
                    else:
                        pm = "You have checked the dummy page"
                elif x == '2':
                    is_loggedin = network.logout(session)
                    if is_loggedin:
                        pm = "Logging out was failed. Try again"
                else:
                    print("Goodbye!")
                    break
            else:
                pm = "Enter 1, 2, or 3"
                    
